//
// Created by asem on 01/04/18.
//

#ifndef SBE201_WORDCOUNT_MAPS_SET_HPP
#define SBE201_WORDCOUNT_MAPS_SET_HPP

#include "bst.hpp"
#include <string>
#include <iostream>

namespace set
{
using WordSet = bst::BSTNode *;

WordSet create()
{
    return nullptr;
}

bool isEmpty(WordSet &wset)
{
    return bst::isEmpty(wset);
}

int size(WordSet &wset)
{
    return bst::size(wset);
}

bool contains(WordSet &wset, std::string item)
{

    return bst::find(wset, item);
}

void remove(WordSet &wset, std::string to_remove)
{
    bst::remove(wset, to_remove);
}

void insert(WordSet &wset, std::string new_item)
{
    if (!bst::find(wset, new_item))
        bst::insert(wset, new_item);
}
bool sizeequal(WordSet &set1, WordSet &set2)
{

    return (set::size(set1) == set::size(set2));
}
bool itemequal(WordSet &set1, WordSet &set2)
{
    if ((!set::isEmpty(set1)) && (!set::isEmpty(set2)))
    {
        if (set1->item == set2->item)
        {
            itemequal(set1->left, set2->left);
            itemequal(set1->right, set2->right);
            return true;
        }
        exit(1);
        return false;
    }
}
bool ifequal(WordSet &set1 ,WordSet &set2)
{
    if (set::sizeequal(set1,set2))
    {
        set::itemequal(set1,set2);
    }
    else
    return false;
}

void printAll(WordSet &wset)
{
    if (wset)
    {
        printAll(wset->left);
        std::cout << wset->item << std::endl;
        printAll(wset->right);
    }
}
void populate(WordSet &s1, WordSet &s2)
{
    if (!set::isEmpty(s1))
    {
        populate(s1->left, s2);
        populate(s1->right, s2);
        set::insert(s2, s1->item);
    }
}
WordSet union_(WordSet &set1, WordSet &set2)
{

    WordSet set3 = create();
    set::populate(set1, set3);
    set::populate(set2, set3);
    return set3;
}
void pintersect(WordSet &s1, WordSet &s2, WordSet &s3)
{
    if (!set::isEmpty(s2))
    {
        pintersect(s1, s2->left, s3);
        pintersect(s1, s2->right, s3);
        if (contains(s1, s2->item))
        {
            set::insert(s3, s2->item);
        }
    }
}

WordSet intersect(WordSet &set1, WordSet &set2)
{
    WordSet set3 = create();
    set::pintersect(set1, set2, set3);
    return set3;
}


bool equals(WordSet &set1, WordSet &set2)
{

        if (set1 == nullptr || set2 == nullptr)
        {
            return false;
        }
        else if (set1->left == nullptr && set1->right == nullptr)
        {
            return (set1->item == set2->item);
            else
            {
                return false;
            }
        }
        else

            equals(set1->left, set2->left);
            equals(set1->right, set2->right);
    
    }
}

#endif //SBE201_WORDCOUNT_MAPS_SET_HPP
