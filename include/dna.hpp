#ifndef DNA_HPP
#define DNA_HPP

#include "arrays.hpp"
namespace dna
{
 char complementaryBase( char base )
 { 
  switch(base)
   { 
    case 'T':
    { 
        return 'A';
    } break;
        
    case 'A':
    {
       return 'T';
    } break;

    case 'G':
    {
       return 'C';
    } break;
    
    default:
    { 
       return 'G';
    } break;
  }
 }
 char * complementarySequence( char *base, int size )
 {
  char *complementary = new char[size];
  for ( int i = size - 1; i >=0; --i )
   { 
     complementary[size - i - 1] = complementaryBase( base[i] );
   }
   
   return complementary;
 }

char *analyzeDNA( char *base, int size, int &countA, int &countC, int &countG, int &countT )
 {
 countA = arrays::countCharacter( base, size, 'A');
 countC = arrays::countCharacter( base, size, 'C');
 countG = arrays::countCharacter( base, size, 'G');
 countT = arrays::countCharacter( base, size, 'T');
  
 return complementarySequence(base, size);
 }
}
 
 
 



#endif // DNA_HPP
