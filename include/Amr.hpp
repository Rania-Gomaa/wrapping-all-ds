#ifndef MEMBER2_HPP
#define MEMBER2_HPP

namespace stack
{
    //int array stack
    struct IntegersStackArray
    {
        int buffer[ 100 ];
        int top = -1;
    };

    void push( IntegersStackArray &stack , int data )
    {
        ++stack.top;
        stack.buffer[stack.top] = data;
    }

    int pop( IntegersStackArray &stack )
    {
        int LIFO = stack.buffer[stack.top];
        --stack.top;
        return LIFO;
    }

    int front( IntegersStackArray &stack )
    {
        return stack.buffer[stack.top];
    }

    bool isEmpty( IntegersStackArray &stack )
    {
        return (stack.top == -1);
    }

    int size( IntegersStackArray &stack )
    {
        return ( stack.top + 1 );
    }

    void clear( IntegersStackArray &stack )
    {
        stack.top = -1;
    }

    //char L.L. stack
    struct CharNodeStack
    {
        char data;
        CharNodeStack *next = nullptr;
    };

    struct CharStackLL
    {
        CharNodeStack *front = nullptr;
    };

    void push( CharStackLL &stack , char data )
    {
        CharNodeStack *newNode = new CharNodeStack{ data , stack.front };
        stack.front = newNode;
    }

    
    }




}

#endif // MEMBER2_HPP
