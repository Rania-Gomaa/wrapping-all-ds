#ifndef ARRAYS_HPP
#define ARRAYS_HPP

#include <iostream>
#include "mathematics.hpp"

namespace arrays
{ 

 void printAll( double *base , int arraySize )
  { 
    for ( int i = 0; i < arraySize; ++i )
    {
      std::cout<<base[i]<<std::endl;
    }
  }
 
 
 double maxArray( double *base, int arraySize )
  { 
    double max = 0;
    
    for ( int i = 0; i < arraySize; ++i )
     {
      if ( base[i] > max )
       { 
         max = base[i];
       }
     }
    return max;
   }
 
 
 double minArray( double *base, int arraySize )
 { 
  double min = 0;
       
    for ( int i = 0; i < arraySize; ++i )
     {
      if ( base[i] < min )
       { 
         min = base[i];
       }
     }
    return min;
  }


 double meanArray( double *base , int arraySize )
 {
  double sum = 0;
  double mean = 0;

    for ( int i = 0; i < arraySize; ++i )
     { 
        sum = sum + base[i];
     }
  return mean = sum/arraySize;
 }
 

 
 double varianceArray( double *base, int arraySize )
 { 
  double mean = 0;
  double sum = 0;
  double variance = 0;
  for ( int i = 0; i < arraySize; ++i )
   {
    mean = meanArray ( &base[i],arraySize );
    sum = sum + ( mean - base[i] );
   }
    variance = mathematics::square ( sum )/arraySize;
    return variance;
 }

int countCharacter( char *basePointer , int size , char query )
 {   
     int counter = 0;
     for ( int i = 0; i < size; ++i )
     {
      if ( basePointer[i] == query )
      {
       counter += 1;
      } }
 }

struct DoubleArray
{ 
 char *basePointer;
 int size;
};

struct CharacterArray
{
 char *basePointer;
 int size;
};

void printAll( DoubleArray array )
 {
  for ( int i = 0; i < array.size; ++i )
   {
      std::cout<<array.basePointer[i]<<std::endl;
   }
 }

double maxArray( DoubleArray array )
 { 
  double max = 0;
    
    for ( int i = 0; i < array.size; ++i )
     {
      if ( array.basePointer[i] > max )
       { 
         max = array.basePointer[i];
       }
     }
    return max;
 }
double minArray( DoubleArray array )
{
   double min = 0;
       
    for ( int i = 0; i < array.size ; ++i )
     {
      if ( array.basePointer[i] < min )
       { 
         min = array.basePointer[i];
       }
     }
    return min;
}
   
double meanArray( DoubleArray array )
{
  double sum = 0;
  double mean = 0;

    for ( int i = 0; i < array.size ; ++i )
     { 
        sum = sum + array.basePointer[i];
     }
  return mean = sum/array.size;

}
double varianceArray( DoubleArray array )
{
  double mean = 0;
  double sum = 0;
  double variance = 0;
  for ( int i = 0; i < array.size ; ++i )
   {
    mean = meanArray ( array );
    sum = sum + ( mean - array.basePointer[i] );
   }
    variance = mathematics::square ( sum )/array.size;
    return variance;
 }
 
  
}  

#endif // ARRAYS_HPP


