#ifndef ECG_HPP
#define ECG_HPP

#include "arrays.hpp"
namespace ecg
{
void analyzeECG( double *base , int arraySize , double &mean, double &variance, double &max, double &min )
 {  
   mean = arrays::meanArray ( &base[0] , arraySize );
   variance = arrays::varianceArray ( &base[0] , arraySize );
   max = arrays::maxArray ( &base[0] , arraySize );
   min = arrays::minArray ( &base[0] , arraySize );
 }

struct ECGArray
{
    double *array;
    int size;
};
 
struct Statistics
{
  double mean;
  double variance;
  double min;
  double max;

};

Statistics analyzeECG( ECGArray ecg)
{
  Statistics str;
  str.mean = arrays::meanArray(ecg.array , ecg.size);
  str.variance = arrays::varianceArray ( ecg.array , ecg.size );
  str.max = arrays::maxArray ( ecg.array , ecg.size );
  str.min = arrays::minArray (ecg.array , ecg.size );

return str;

}

}
  
   
#endif // ECG_HPP
