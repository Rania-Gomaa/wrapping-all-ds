//
// Created by asem on 01/04/18.
//

#ifndef SBE201_WORDCOUNT_MAPS_BST_HPP
#define SBE201_WORDCOUNT_MAPS_BST_HPP

#include <iostream>
#include <string>
#include <queue>
using namespace std;
    
namespace bst
{

struct BSTNode
{
    std::string item;
    BSTNode* left;
    BSTNode* right;
};

using Tree = BSTNode *;

Tree create()
{
    return nullptr;
}

bool isEmpty( Tree node )
{
return node ==nullptr ; 
}

bool isLeaf( Tree node )
{
return ( node -> left == nullptr && node -> right == nullptr) ;
}

int size( Tree node )
{
 if (!isEmpty(node))
 return 1 + size(node -> left ) + size(node -> right );
}

bool find( Tree tree, std::string item )
{
    if (isEmpty(tree))
    return false ; 
    else 
    {
        if (item == tree -> item )
        return true ; 
        else if ( item < tree -> item )
        return find(tree -> left , item );
        else 
        return find(tree -> right , item) ; 
    }
}

void insert( Tree &tree, std::string item )
{
if (isEmpty(tree))
tree = new BSTNode {item , nullptr , nullptr};
else 
{
    if (item < tree -> item )
    insert(tree-> left , item );
    else insert(tree->right , item );
}
}

Tree minNode( Tree tree )
{
if (!isEmpty(tree->left))
{
 minNode(tree->left);

}
else return tree;
}

Tree maxNode( Tree tree )
{
if (!isEmpty(tree->right))
{
    return maxNode(tree->right);
}
return tree;
}

void remove( Tree &tree, std::string item )
{
  if ( isEmpty( tree )) return;

    else if ( item == tree->item )
    {
        if ( !isEmpty( tree->left ) && !isEmpty( tree->right ))
        {
            BSTNode *minRight = minNode( tree->right );
            tree->item = minRight->item;
            remove( tree->right, minRight->item );
        } else
        {
            BSTNode *discard = tree;

            if ( isLeaf( tree ))
                tree = nullptr;
            else if ( !isEmpty( tree->left ))
                tree = tree->left;
            else
                tree = tree->right;

            delete discard;
        }

    } else if ( item < tree->item )
        remove( tree->left, item );
    else remove( tree->right, item );
}


void clear( Tree &tree )
{
if ( !isEmpty( tree ))
    {
        clear( tree->left );
        clear( tree->right );
        delete tree;
        tree = nullptr;
    }
}

void preorder( Tree tree )
{
if(tree)
{
    std::cout<< "[" << tree->item << "]";
    preorder(tree->left);
    preorder(tree->right);
}
}

void inorder( Tree tree )
{
 if (tree )
 {
     inorder(tree->left);
     std::cout<< "[" << tree->item << "]";
     inorder(tree->right);
 }
}

void postorder( Tree tree )
{
if(tree)
{
    postorder(tree->left);
    postorder(tree->right);
    std::cout<< "[" << tree->item << "]";
}
}

void breadthFirst( Tree tree )
{
queue<Tree>q;
q.push(tree);
while(!q.empty() && !isEmpty(tree))
{
    Tree node=q.front();
    cout<<"["<<q.front()->item<<"]";
    q.pop();
    if(!isEmpty(node->left))
    {
        q.push(node->left);
    }
    if(!isEmpty(node->right))
    {
        q.push(node->right);
    }
    
}

}

}


#endif //SBE201_WORDCOUNT_MAPS_BST_HPP_HPP
