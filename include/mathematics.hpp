#ifndef MATHEMATICS_HPP
#define MATHEMATICS_HPP

#include <cmath> // for std::sqrt

namespace mathematics
{

double calculation( double a , double b , char operation )
 {
  if ( operation == '+' )
   { 
     return a+b;
   }
  else if ( operation == '-')
   {
     return a-b; 
   }
  else if ( operation == '*' )
   { 
     return a*b;
   }
  else if ( operation == '/' )
   {
     return a/b;
   }
  else 
   { 
    return 0;
   }

 }

double heron( double a , double b , double c )
 { 
  double s = 0; //where s is the semiperimeter
  double A = 0; //where A is the triangle area
  s = (a+b+c)/2;
  A = s*(s-a)*(s-b)*(s-c);
  return std::sqrt(A);
  
    
 }

double square( double a )
 {
  double square = a*a;
  
  return square;
 }

struct Triangle
{
    double a;
    double b;
    double c;
};


double heron( Triangle t )
{
  double s = 0; //where s is the semiperimeter
  double A = 0; //where A is the triangle area
  s = (t.a + t.b + t.c)/2;
  A = s*(s-t.a)*(s-t.b)*(s-t.c);
  return std::sqrt(A);
  

}

#endif // MATHEMATICS_HPP
