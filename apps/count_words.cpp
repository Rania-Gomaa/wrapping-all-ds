//
// Created by asem on 01/04/18.
//

#include "map.hpp"
#include "helpers3.hpp"

int main(int argc, char **argv)
{
    if (argc == 2)
    {

      std::vector<std::string> word = getFileWords(argv[1]);

        map::WordMap WordCounter = map::create();

        for (int i = 0; i < word.size(); ++i)
        {
            map::value(WordCounter,word[i])++;
        }
        map::printAll(WordCounter);
    }
    else
    std::cout<<"error usage"<<std::endl;

    return 0;
}
