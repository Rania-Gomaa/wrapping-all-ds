#include "mathematics.hpp" // for mathematics::heron
#include <iostream> // for std::cout
#include <algorithm> // for std::atof

// We need main function here!
int main( int argc , char **argv )
{
  //double a = 0, b = 0, c = 0;
 
  mathematics::Triangle t{ 0 , 0 , 0 };
  
  std::cin >> t.a >> t.b >> t.c;
   
   t.a = std::atof( argv[1] );
   t.b = std::atof( argv[2] );
   t.c = std::atof( argv[3] );
  
  std::cout << mathematics::heron( t ) << std::endl;
 
 //std::cout << mathematics::heron( a , b , c ) << std::endl;
}
