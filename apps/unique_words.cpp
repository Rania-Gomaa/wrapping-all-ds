//
// Created by asem on 01/04/18.
//

#include "set.hpp"
#include "helpers3.hpp"

int main(int argc, char **argv)
{
    if (argc == 2)
    {
        std::vector<std::string> words = getFileWords(argv[1]);
        set::WordSet wset = set::create();

        for (int i = 0; i < words.size(); ++i)
        {
            set::insert(wset, words[i]);
        }
        set::printAll(wset);

        
    }
    else 
    std::cout<<"error usage"<<std::endl;

    return 0;
}
